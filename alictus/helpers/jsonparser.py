import json


class JSONParser:

    #JSON File
    googlemimetypereferencesfile = "alictus/static/googlemimetypereferences.json"
    slacknotificationsfile = "alictus/static/notifications.json"
    slackchannelsfile = "alictus/static/slackchannels.json"
    slackcredentialsfile = "alictus/static/slackcredentials.json"

    def __init__(self):
        with open(self.googlemimetypereferencesfile, 'r') as file:
            self.googlemimetypejson = json.loads(file.read())

        with open(self.slacknotificationsfile, 'r') as file:
            self.slacknotificationsjson = json.loads(file.read())

        with open(self.slackchannelsfile, 'r') as file:
            self.slackchannelsjson = json.loads(file.read())

        with open(self.slackcredentialsfile, 'r') as file:
            self.slackcredentialsjson = json.loads(file.read())

    """
    This function parses the google mime type references.json file and extracts
    data types for processing them.
    """
    @classmethod
    def getmimetypeofdescription(cls, description):
        parser = JSONParser()
        jsondata = parser.googlemimetypejson
        mimetypevalue = ""
        for key, value in jsondata["mimetypes"].items():
            if key == description:
                mimetypevalue = value
        return mimetypevalue

    @classmethod
    def getdatatypes(cls, datatype):
        parser = JSONParser()
        jsondata = parser.googlemimetypejson
        datatypes = []
        for key, value in jsondata[datatype].items():
            datatypes.append(key)
        return datatypes

    @classmethod
    def getvaluesofdatatypes(cls, datatype):
        parser = JSONParser()
        jsondata = parser.googlemimetypejson
        datatypes = []
        for key, value in jsondata[datatype].items():
            datatypes.append(value)
        return datatypes

    @classmethod
    def getscopes(cls, scopetype):
        parser = JSONParser()
        jsondata = parser.googlemimetypejson
        return ""

    @classmethod
    def getslackcredentials(cls):
        # We already have the path
        parser = JSONParser()
        credential = parser.slackcredentialsjson
        return credential['SLACK_TOKEN']

    @classmethod
    def getslacksigningsecret(cls):
        parser = JSONParser()
        signingsecret = parser.slackcredentialsjson
        return signingsecret['SIGNING_SECRET']

    @classmethod
    def getnotificationmessage(cls, messagetype):
        parser = JSONParser()
        jsondata = parser.slacknotificationsjson
        return jsondata[messagetype]