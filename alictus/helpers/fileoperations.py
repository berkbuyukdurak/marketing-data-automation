import os
import requests
from alictus.models.campaign import Campaign
import openpyxl

"""
This function contains all of the file operations.
"""

class FileOperations:

    @classmethod
    def findspecificfiles(cls, filetype):
        foundfiles = []
        foundfiletypes = []
        files = os.listdir('files')
        for file in files:
            for typ in filetype:
                if file.endswith('.' + typ):
                    foundfiles.append(file)
                    foundfiletypes.append(typ)
        return foundfiles, foundfiletypes

    @classmethod
    def parsedatafile(cls, foundfiles):
        # opening downloaded file in read mode
        root_path = "./downloadedfiles/"
        campaigns = []
        for file in foundfiles:
            workbook = openpyxl.load_workbook(root_path + file)
            ws = workbook.active

            for row in ws.iter_rows(min_row=2, max_row=ws.max_row, values_only=True, max_col=6):
                # Creationg campaign object
                campaign = Campaign(row[0], row[1], row[2], row[3], row[4], row[5])
                campaigns.append(campaign)

        return campaigns