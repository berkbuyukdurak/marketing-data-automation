
"""
This object will keep all of the campaign information.
"""

class Campaign:

    def __init__(self, name, impression, clicks, ctr, cpc, appinstall):
        self.name = name
        self.impression = impression
        self.clicks = clicks
        self.ctr = ctr
        self.cpc = cpc
        self.appinstall = appinstall

    # Getter Methods
    # Getter of Name
    def get_name(self):
        return self.name

    # Getter of Impressions
    def get_impressions(self):
        return self.impression

    # Getter of Clicks
    def get_clicks(self):
        return self.clicks

    # Getter of ctr
    def get_ctr(self):
        return self.ctr

    # Getter of cpc
    def get_cpc(self):
        return self.cpc

    #Getter of appinstall
    def get_appinstall(self):
        return self.appinstall