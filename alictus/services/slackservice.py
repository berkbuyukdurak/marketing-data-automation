import slack
from alictus.helpers.jsonparser import JSONParser


class Slack:

    @classmethod
    def createClient(cls):
        slacktoken = JSONParser.getslackcredentials()
        slackclient = slack.WebClient(token=slacktoken)
        return slackclient