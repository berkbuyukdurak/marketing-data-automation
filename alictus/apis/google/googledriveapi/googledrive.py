from alictus.helpers.jsonparser import JSONParser
from alictus.enums.googlereferenceenums import DataType
import requests

import os, io
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload

class GoogleDriveAPI:

    foldermimetype = JSONParser.getmimetypeofdescription(DataType.FOLDER.value)
    uploadfilemimetype = JSONParser.getvaluesofdatatypes(DataType.TEXT.value)
    uploadfiledatatype = JSONParser.getdatatypes(DataType.TEXT.value)

    @classmethod
    def createfolder(cls, service, foldername):
        flag = cls.checkfoldername(service, foldername)
        if type(flag) != type('str'):
            # Creating metadata for creating folder
            file_metadata = {
                'name': foldername,
                'mimeType': cls.foldermimetype
            }
            file = service.files().create(body=file_metadata, fields='id').execute()
            done = "File named " + foldername + " is created."
            print(done)
            return True
        else:
            error = foldername + " is already created."
            print(error)
            return False

    @classmethod
    def checkfoldername(cls, service, foldername):
        page_token = None
        while True:
            response = service.files().list(q="mimeType='application/vnd.google-apps.folder'",
                                                  spaces='drive',
                                                  fields='nextPageToken, files(id, name)',
                                                  pageToken=page_token).execute()
            for file in response.get('files', []):
                if foldername == file.get('name'):
                    foundfileid = file.get('id')
                    print("Found file ID --> ", foundfileid)
                    return foundfileid
            return True

    @classmethod
    def getfiles(cls, service, filetypes):
        foundfiles = []
        for filetype in filetypes:
            try:
                filetypeindex = cls.uploadfiledatatype.index(filetype)
            except ValueError:
                filetypeindex = -1
            if filetypeindex >= 0:
                # Retrieving MIME type
                mime_type = cls.uploadfilemimetype[filetypeindex]
                page_token = None
                while True:
                    response = service.files().list(q="mimeType='" + mime_type + "'",
                                                    spaces='drive',
                                                    fields='nextPageToken, files(id, name)',
                                                    pageToken=page_token).execute()
                    for file in response.get('files', []):
                        # Process change
                        filename = file.get('name')
                        if filename.endswith('.' + filetype):
                            foundfiles.append(file)
                    page_token = response.get('nextPageToken', None)
                    if page_token is None:
                        return foundfiles
            else:
                break

    @classmethod
    def findfileidinspecificfolder(cls, service, folderid, filename):
        foundfileid = None
        query = f"parents = '{folderid}'"

        try:
            response = service.files().list(q=query).execute()
        except requests.HTTPError as exception:
            return "not found"


        files = response.get('files')
        nextPageToken = response.get('nextPageToken')

        while nextPageToken:
            response = service.files().list(q=query, pageToken = nextPageToken).execute()
            files.extend(response.get('files'))
            nextPageToken.response.get('nextPageToken')

        for file in files:
            if file.get('name') == filename:
                foundfileid = file.get('id')

        if foundfileid is None:
            return None
        else:
            return foundfileid


    @classmethod
    def downloadfiles(cls, service, filetypes):
        files = cls.getfiles(service, filetypes)
        for file in files:
            request = service.files().get_media(fileId=file.get('id'))

            fh = io.BytesIO()
            downloader = MediaIoBaseDownload(fd=fh, request=request)
            done = False

            while not done:
                status, done = downloader.next_chunk()
                print("Download progress {0}".format(status.progress() * 100))

            fh.seek(0)

            # Checking directory before write it
            download_file_path = './downloadedfiles'
            isexist = os.path.exists(download_file_path)

            if not isexist:
                os.makedirs(download_file_path)

            file_name = file.get('name')
            with open(os.path.join(download_file_path, file_name), 'wb') as f:
                f.write(fh.read())
                f.close()


        return True

    @classmethod
    def uploadfiles(cls, service, folderid, files, filetypes):
        for file in files:
            # Getting mime_type
            filetypeindex = None
            for filetype in filetypes:
                try:
                    filetypeindex = cls.uploadfiledatatype.index(filetype)
                except ValueError:
                    filetypeindex = -1

            if filetypeindex >= 0:
                # Retrieving MIME type
                mime_type = cls.uploadfilemimetype[filetypeindex]

                # Creating file_metada
                file_metadata = {
                    'name': file,
                    "parents": [folderid]
                }
                file_route = "files/" + file

                media = MediaFileUpload(file_route, mimetype=mime_type)
                file = service.files().create(
                    body=file_metadata,
                    media_body=media,
                    fields='id').execute()
                return "File is uploaded"
            else:
                break