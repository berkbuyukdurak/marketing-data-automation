from alictus.helpers.jsonparser import JSONParser
from alictus.enums.googlereferenceenums import DataType

class GoogleSheetsAPI:
    spreadsheetmimetype = JSONParser.getmimetypeofdescription(DataType.SPREADSHEET.value)

    @classmethod
    def createspreadsheetsfile(cls, service, folderid, file_name):

        file_metadata = {
            'name': file_name,
            'parents': [folderid],
            'mimeType': cls.spreadsheetmimetype
        }
        created_sheet = service.files().create(body=file_metadata).execute()
        return created_sheet.get('id')

    """
    This is the core function to create and update spreadsheets
    """
    @classmethod
    def createcampaignspreadsheets(cls, google_service, google_sheets_service, folderid, campaigns):
        # File creation

        for i in range(len(campaigns)):
            campaign = campaigns[i]
            file_name = "Campaign " + campaign.get_name()
            createdspreadsheetfileid = cls.createspreadsheetsfile(google_service, folderid, file_name)
            # Inserting Data to the Created files
            cell_range_insert = 'A1'
            values = (
                ('Campaign Name', 'Total Impression', 'Total Clicks', 'CTR (%)', 'CPC', 'Total App Install', 'Total Budget'),
                (campaign.get_name(), campaign.get_impressions(), campaign.get_clicks(), campaign.get_ctr(), campaign.get_cpc(), campaign.get_appinstall()),
            )
            value_range_body = {
                'majorDimension': 'ROWS',
                'values': values
            }
            google_sheets_service.spreadsheets().values().update(
                spreadsheetId=createdspreadsheetfileid,
                valueInputOption='USER_ENTERED',
                range=cell_range_insert,
                body=value_range_body
            ).execute()

            # Calculating total budget with the spreadsheet formula
            calculate_totalbudget = {
                "requests": [
                    {
                        "repeatCell": {
                            "range": {
                                "startRowIndex": 1,
                                "endRowIndex": 2,
                                "startColumnIndex": 6,
                                "endColumnIndex": 7
                            },
                            "cell": {
                                "userEnteredValue": {
                                    "formulaValue": "=PRODUCT(C2*E2)"
                                }
                            },
                            "fields": "userEnteredValue"
                        }
                    }
                ]
            }

            google_sheets_service.spreadsheets().batchUpdate(
                spreadsheetId=createdspreadsheetfileid,
                body=calculate_totalbudget
            ).execute()
            print("Spreadsheet is created for Campaign " + campaigns[i].get_name())
        return True


    @classmethod
    def readtotalbudgetfromspreadsheet(cls, service, fileid):
        response = service.spreadsheets().values().get(
            spreadsheetId=fileid,
            majorDimension='ROWS',
            range='Sheet1!G2:G2'
        ).execute()
        return response.get('values')[0][0]