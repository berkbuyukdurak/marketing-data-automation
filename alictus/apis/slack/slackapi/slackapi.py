
class SlackAPI:

    @classmethod
    def sendmessage(cls, client, message):
        client.chat_postMessage(channel="#marketing-insight-automation", text=message)

    @classmethod
    def sendcommandresponse(cls, client, channel_id, message):
        client.chat_postMessage(channel=channel_id, text=message)