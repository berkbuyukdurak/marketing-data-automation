from alictus.helpers.jsonparser import JSONParser
from alictus.enums.googlereferenceenums import DataType


class SlackEventsAPI:

    acceptedtypes = JSONParser.getdatatypes(DataType.TEXT.value)
    """
    This function checks the uploaded files and it only accepts the reference types.
    """
    @classmethod
    def checkMessages(cls, payload):
        # Getting event from payload
        event = payload.get('event', {})
        files = event.get('files')
        if files is not None:
            print("Info --> Incoming message is file notification message. Checking its type if it is one of them --> "
                  , cls.acceptedtypes)
            # Checking incoming message if it contains accepted types
            for attr in files:
                filetype = attr.get('filetype')
                file_name = attr.get('name')
                if filetype in cls.acceptedtypes and file_name == "dataset.xlsx":
                    print("Valid file type [" + filetype + "] is found!")
                    return True
            return False
        else:
            print("Info --> Incoming message is not file notification message.")
            return False