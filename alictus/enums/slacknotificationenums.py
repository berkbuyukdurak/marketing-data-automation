from enum import Enum

class Notifications(str, Enum):
    START_PROCESS = 'start_process'
    DOWNLOAD_COMPLETED = 'download_completed'
    FOLDER_CREATED = 'folder_created'
    END_PROCESS = 'end_process'