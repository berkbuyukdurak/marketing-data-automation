from enum import Enum


class DataType(str, Enum):
    TEXT = 'text'
    FOLDER = 'folder'
    SPREADSHEET = 'spreadsheet'