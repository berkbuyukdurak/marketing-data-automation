from flask import Flask, request, Response
from operations import Operations
from slackeventsapi import SlackEventAdapter
from alictus.helpers.jsonparser import JSONParser
from alictus.enums.googlereferenceenums import DataType
from alictus.enums.slacknotificationenums import Notifications
from alictus.apis.slack.slackeventsapi.slackeventsapi import SlackEventsAPI

# Creating flask app and creating slack event adapter
app = Flask(__name__)
slack_event_adapter = SlackEventAdapter(JSONParser.getslacksigningsecret(), '/slack/events', app)

"""
Doesn't work
@app.route('/sendnotification', methods=['POST', 'GET'])
def sendnotification():
    print("-------------")
    data = request.message
    print(data)
    #operations = Operations()
    #slack_client = operations.slackservice.createClient()
    #message = JSONParser.getnotificationmessage(payload.message_type)
    #operations.slackapi.sendmessage(slack_client, message)
    return "ok"
"""

@app.route('/start')
def startingpoint():
    operations = Operations()
    google_service = operations.creategoogledriveapiconnection()
    operations.uploadfiletodrive(google_service)
    return 'File is uploaded to the Google Drive. ' \
           'Now, you should get an slack notification about file upload and automation process should begin. ' \
           'You can close this window now.'


"""
Calculate Budget command route
"""
@app.route('/calculate_budget', methods = ['POST'])
def calculate_budget():
    operations = Operations()
    slack_client = operations.slackservice.createClient()
    data = request.form
    channel_id = data.get('channel_id')
    parameter = data.get('text')
    if parameter:
        message = "I got the command! Working on it..."
        operations.slackapi.sendcommandresponse(slack_client, channel_id, message)
        total_budget = operations.calculate_budget(parameter)
        if total_budget is not None:
            message = "Campaign " + parameter + " has total budget of " + total_budget
            operations.slackapi.sendcommandresponse(slack_client,channel_id,message)
        else:
            message = "Campaign couldn't found! Please enter a valid campaign name."
            operations.slackapi.sendcommandresponse(slack_client,channel_id,message)

    else:
        message = "Please enter a valid campaign name - Valid Usage --> /calculate_budget <campaign_name>"
        operations.slackapi.sendcommandresponse(slack_client, channel_id, message)
    return Response(), 200


"""
Slack Event Listener
This function is the entry point of the automation.
"""
@slack_event_adapter.on('message')
def message(payload):
    checkmessage_flag = SlackEventsAPI.checkMessages(payload)
    if checkmessage_flag:
        operations = Operations()
        return operations.initiateOperations(payload)

if __name__ == '__main__':
    app.run()