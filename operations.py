from __future__ import annotations
from alictus.services.slackservice import Slack
from alictus.services.googleservice import Google
from alictus.apis.google.googledriveapi.googledrive import GoogleDriveAPI
from alictus.apis.google.googlesheetsapi.googlesheets import GoogleSheetsAPI
from alictus.apis.slack.slackapi.slackapi import SlackAPI
from alictus.apis.slack.slackeventsapi.slackeventsapi import SlackEventsAPI
from alictus.helpers.jsonparser import JSONParser
from alictus.helpers.fileoperations import FileOperations
from alictus.enums.slacknotificationenums import Notifications
from alictus.enums.googlereferenceenums import DataType
import requests



requestRoute = "http://d59a-94-54-48-104.ngrok.io/sendnotification"

class Operations:
    """
    This Facade class provides a simple interface to the complex logic of one or
    several subsystems. The Facade delegates the client requests to the
    appropriate objects within the subsystem. The Facade is also responsible for
    managing their lifecycle. All of this shields the client from the undesired
    complexity of the subsystem.
    """

    # It will be used as starting of the automation
    flag = False

    def __init__(self):
        self.googleservice = Google()
        self.slackservice = Slack()
        self.googledriveapi = GoogleDriveAPI()
        self.googlesheetsapi = GoogleSheetsAPI()
        self.slackapi = SlackAPI()
        self.slackeventsapi = SlackEventsAPI()
        self.jsonparser = JSONParser()
        self.fileoperations = FileOperations()

    def initiateOperations(self, payload):
        google_drive_service = self.creategoogledriveapiconnection()
        google_sheets_service = self.creategooglesheetsapiconnection()
        slack_client = self.slackservice.createClient()

        # Process completed. Send message
        startmessage = self.jsonparser.getnotificationmessage(Notifications.START_PROCESS.value)
        self.slackapi.sendmessage(slack_client, startmessage)

        #Process 1) Downloading the uploaded file
        foundfiles, foundfiletypes = self.fileoperations.findspecificfiles(self.jsonparser.getdatatypes(DataType.TEXT.value))
        download_flag = self.googledriveapi.downloadfiles(google_drive_service, foundfiletypes)

        if download_flag:

            # Process completed. Send message
            downloadmessage = self.jsonparser.getnotificationmessage(Notifications.DOWNLOAD_COMPLETED.value)
            self.slackapi.sendmessage(slack_client, downloadmessage)

            campaignsfolder = "Campaigns"
            self.googledriveapi.createfolder(google_drive_service, campaignsfolder)

            # Process completed. Send message
            foldermessage = self.jsonparser.getnotificationmessage(Notifications.FOLDER_CREATED.value)
            self.slackapi.sendmessage(slack_client, foldermessage)
            folderid = self.googledriveapi.checkfoldername(google_drive_service, campaignsfolder)

            foundfiles, foundfiletypes = self.fileoperations.findspecificfiles(self.jsonparser.getdatatypes(DataType.TEXT.value))
            campaigns = self.fileoperations.parsedatafile(foundfiles)
            self.googlesheetsapi.createcampaignspreadsheets(google_drive_service, google_sheets_service, folderid, campaigns)

            # Process completed. Send message
            endmessage = self.jsonparser.getnotificationmessage(Notifications.START_PROCESS.value)
            self.slackapi.sendmessage(slack_client, endmessage)
        else:
            print("Could not initiate the automation.")

    def creategoogledriveapiconnection(self):
        CLIENT_SECRET_FILE = 'alictus/static/googlecredentials.json'
        API_NAME = 'drive'
        API_VERSION = 'v3'
        SCOPES = ['https://www.googleapis.com/auth/drive']
        service = self.googleservice.Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)
        return service

    def creategooglesheetsapiconnection(self):
        CLIENT_SECRET_FILE = 'alictus/static/googlecredentials.json'
        API_NAME = 'sheets'
        API_VERSION = 'v4'
        SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
        service = self.googleservice.Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)
        return service

    def uploadfiletodrive(self, service):
        folderid = self.googledriveapi.checkfoldername(service, "Initialize Process File")
        foundfiles, foundfiletypes = self.fileoperations.findspecificfiles(self.jsonparser.getdatatypes(DataType.TEXT.value))
        self.googledriveapi.uploadfiles(service, folderid, foundfiles, foundfiletypes)
        return

    def downloadfilefromdrive(self, service):
        foundfiles, foundfiletypes = self.fileoperations.findspecificfiles(self.jsonparser.getdatatypes(DataType.TEXT.value))
        flag = self.googledriveapi.downloadfiles(service, foundfiletypes)
        if flag:
            self.fileoperations.parsedatafile(foundfiles)
        return


    def calculate_budget(self, parameter):
        google_drive_service = self.creategoogledriveapiconnection()
        google_sheets_service = self.creategooglesheetsapiconnection()
        foundfiles, foundfiletypes = self.fileoperations.findspecificfiles(self.jsonparser.getdatatypes(DataType.TEXT.value))
        folderid = self.googledriveapi.checkfoldername(google_drive_service, "Campaigns")
        file_name = "Campaign " + parameter
        foundfileid = self.googledriveapi.findfileidinspecificfolder(google_drive_service, folderid, file_name)
        if foundfileid is not None:
            total_budget = self.googlesheetsapi.readtotalbudgetfromspreadsheet(google_sheets_service, foundfileid)
            return total_budget
        else:
            return None