# Alictus Case - Market Insights Automation #
This project processes the desired campaign dataset and creates a new
spreadsheet for each campaign. While creating this table, the total budget calculation is also made.
This calculation was made with the spreadsheet formula. While this process is 
running, you can follow the process via the Slack bot. At the end of each job, a notification message is sent
to you. When the automation work is finished, it is explained which commands you can use.

### Used APIs ###

* Google Drive API
* Google Sheets API
* Slack API
* Slack Events API

### How do I get set up? ###

* ## Configuration
    ##### After cloning the project, please run pip install -r requirements.txt
    ##### Project will with "flask run" command. However, if you encounter any problem with running the code, try this;
        set FLASK_ENV=development
        set FLASK_APP=main.py
    ##### then run with flask run.
    ##### Also, you need a ngrok in your project. Please go to the website, register and ngrok.exe --> https://www.ngrok.com/ to work with your local without need
    ##### any domain. However, each time you close ngrok, you have start the ngrok.exe again. You have to change the address when you restart the ngrok.
    ##### This is the issue about free subscription. Also, you should add ngrok auth key to the ngrok.exe to able to make
    ##### channel commands.

* ## Summary of set up
    ##### To set up Google Drive APIs, please replace alictus/static/googlecredentials.json file with your own credentials. Otherwise, you cannot use this project.
    ##### To set up Slack APIs, please replace alictus/static/slackcredentials.json file with your own credentials. Otherwise, you cannot use this project.
        More on Slack API
            You need to configure your own Slack App to use this project.
    ##### After configuring the API files, you are ready.


